import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sk_commerce_app/features/authentication/controllers/onboarding_controller.dart'; 
import 'package:sk_commerce_app/features/authentication/widgets/onboarding_dot_navigation.dart';
import 'package:sk_commerce_app/features/authentication/widgets/onboarding_next_button.dart';
import 'package:sk_commerce_app/features/authentication/widgets/onboarding_page.dart';
import 'package:sk_commerce_app/features/authentication/widgets/onboarding_skip.dart';
import 'package:sk_commerce_app/utils/constants/image_strings.dart';
import 'package:sk_commerce_app/utils/constants/text_string.dart';

class OnBoardingScreen extends StatelessWidget {
  const OnBoardingScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final controller = Get.put(OnBoardingController());
    return Scaffold(
      body: Stack(
        children: [
          PageView(
            controller: controller.pageController,
            onPageChanged: controller.updatePageIndicator,
            children: const [
            OnBoardingPage(
              image: SkImages.onBoardingImage1,
              title: SkTexts.onBoardingTitle1,
              subTitle: SkTexts.onBoardingSubTitle1,
            ),
            OnBoardingPage(
              image: SkImages.onBoardingImage2,
              title: SkTexts.onBoardingTitle2,
              subTitle: SkTexts.onBoardingSubTitle2,
            ),
            OnBoardingPage(
              image: SkImages.onBoardingImage3,
              title: SkTexts.onBoardingTitle3,
              subTitle: SkTexts.onBoardingSubTitle3,
            ),
          ]),
          const OnBoardingSkip(),
          const OnBoardingDotNavigation(),
          const OnboardingNextButton()
        ],
      ),
    );
  }
}

 

 
