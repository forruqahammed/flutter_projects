// import 'package:flutter/material.dart';
// import 'package:get/get.dart';

// class OnBoardingController extends GetxController {
//   static OnBoardingController get instance => Get.find();

//   //variables
//   final pageController = PageController();
//   Rx<int> currentPageIndex = 0.obs;

//   //Update Current Index when page scroll
//   void updatePageIndicator(index) => currentPageIndex = index;
 
//   //Jump to the specific dot selected page
//   void dotNavigationClick(index) {
//     currentPageIndex.value = index;
//     pageController.jumpTo(index);
//   }

//   //Update current index & jump to the nex page
//   void nextPage() {
//     if(currentPageIndex.value == 2){

//     } else {
//       double page = currentPageIndex.value + 1;
//       pageController.jumpTo(page);
//     }
//   }

//   //Update current index and jump to the last page
//   void skipPage() {
//     currentPageIndex.value = 2;
//     pageController.jumpTo(2);
//   }
// }


import 'package:flutter/material.dart';
import 'package:get/get.dart';

class OnBoardingController extends GetxController {
  static OnBoardingController get instance => Get.find();

  // Variables
  final pageController = PageController();
  Rx<int> currentPageIndex = 0.obs;

  // Update Current Index when page scroll
  void updatePageIndicator(int index) => currentPageIndex.value = index;

  // Jump to the specific dot-selected page
  void dotNavigationClick(int index) {
    currentPageIndex.value = index;
    pageController.jumpToPage(index); // Use jumpToPage with index
  }

  // Update current index & jump to the next page
  void nextPage() {
    if (currentPageIndex.value < 2) {
      currentPageIndex.value += 1;
      pageController.animateToPage(
        currentPageIndex.value,
        duration: Duration(milliseconds: 300),
        curve: Curves.easeInOut,
      );
    }
  }

  // Update current index and jump to the last page
  void skipPage() {
    currentPageIndex.value = 2;
    pageController.jumpToPage(2); // Use jumpToPage with index
  }
}