import 'package:flutter/material.dart';
import 'package:sk_commerce_app/features/authentication/controllers/onboarding_controller.dart';
import 'package:sk_commerce_app/utils/constants/colors.dart';
import 'package:sk_commerce_app/utils/constants/size.dart';
import 'package:sk_commerce_app/utils/device/device_utills.dart';
import 'package:sk_commerce_app/utils/helpers/helper_functions.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

class OnBoardingDotNavigation extends StatelessWidget {
  const OnBoardingDotNavigation({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    final controller = OnBoardingController.instance;
    final dark = SkhelperFunctions.isDarkMode(context);
    return Positioned(
      bottom: SkDeviceUtils.getBottomNavigationBarHeight() + 25,
      left: SkSize.defaultSpace,
      child: SmoothPageIndicator(
        controller: controller.pageController,
        onDotClicked: controller.dotNavigationClick,
        count: 3,
        effect: ExpandingDotsEffect(
            activeDotColor: dark ? SkColors.white : SkColors.dark,
            dotHeight: 6),
      ),
    );
  }
}