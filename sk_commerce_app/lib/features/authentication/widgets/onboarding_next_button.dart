import 'package:flutter/material.dart';
import 'package:iconsax/iconsax.dart';
import 'package:sk_commerce_app/features/authentication/controllers/onboarding_controller.dart';
import 'package:sk_commerce_app/utils/constants/colors.dart';
import 'package:sk_commerce_app/utils/constants/size.dart';
import 'package:sk_commerce_app/utils/device/device_utills.dart';
import 'package:sk_commerce_app/utils/helpers/helper_functions.dart';

class OnboardingNextButton extends StatelessWidget {
  const OnboardingNextButton({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    final controller = OnBoardingController.instance;
    final dark = SkhelperFunctions.isDarkMode(context);
    return Positioned(
      right: SkSize.defaultSpace,
      bottom: SkDeviceUtils.getBottomNavigationBarHeight(),
      child: ElevatedButton(
        onPressed: () => controller.nextPage(),
        style: ElevatedButton.styleFrom(shape: const CircleBorder(), backgroundColor: dark? SkColors.primary : Colors.black),
        child: const Icon(Iconsax.arrow_right_3)
        ),
    );
  }
}  