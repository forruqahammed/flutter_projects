 
import 'package:flutter/material.dart';
import 'package:sk_commerce_app/features/authentication/controllers/onboarding_controller.dart';
import 'package:sk_commerce_app/utils/constants/size.dart';
import 'package:sk_commerce_app/utils/device/device_utills.dart';

class OnBoardingSkip extends StatelessWidget {
  const  OnBoardingSkip({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    final controller = OnBoardingController.instance;
    return Positioned(
      right: SkSize.defaultSpace,
      top: SkDeviceUtils.getAppBarHeight(),
      child: TextButton(
        onPressed: () => controller.skipPage(),
        child: const Text("Skip"),
      ),
    );
  }
}