
class SkPricingCalculator {

  //Calculator price based on tax and shipping
  static double calculateTotalPrice(double productPrice, String location){
    double taxRate = getTaxRateForLocation(location);
    double taxAmount = productPrice * taxRate;
    double shippingCost = getShippingCost(location);
    double totalPrice = productPrice + taxAmount + shippingCost;
    return totalPrice;
  }

  static String calculateShippingCost(double productPrice, String location) {
    double shippingCost = getShippingCost(location);
    return shippingCost.toStringAsFixed(2);
  }

  static String calculateTax(double productPrice, String location) {
    double taxRate = getTaxRateForLocation(location);
    double taxAmount = productPrice * taxRate;
    return taxAmount.toStringAsFixed(2);
  }
  static double getTaxRateForLocation(String location) {
    // Lookup the tax rate for the given location from tax rate database of Api
    //Return the appropriate tax rate for the given location
    return 0.10; //Example tax rate of 10% 
  }

  static double getShippingCost(String location){
    //Lookup the shipping cost for the given location using a shipping rate api
    //Calculate the shipping cost based on various factors like distance, weight etc
    return 5.00; //Example shipping cost of $5
  }
}