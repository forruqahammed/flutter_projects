class SkTexts {
  static const String onBoardingTitle1 = "Coose your product";
  static const String onBoardingTitle2 = "Select Payment Method";
  static const String onBoardingTitle3 = "Deliver at your door step";


  static const String onBoardingSubTitle1 = "Welcome to a world of limitless choices - Your Perfect Product Awaits";
  static const String onBoardingSubTitle2 = "For Seamless Transactions, Choose Your Payment Path - Your Convenience, Our Priority!";
  static const String onBoardingSubTitle3 = "From Our Doorstep to Yours - Swift, Secure and Contactless Delivery!";

  static const String homeAppbarTilte = "Good day for shopping";
  static const String homeAppbarSubTitle = "Forruq Ahammed";
}
