
class SkImages {

  // App Logos
  static const String darkAppLogo = "assets/logos/splash_logo_black.png";
  static const String lightAppLogo = "assets/logos/splash_logo_white.png";

  // Social Logos
  static const String google = "assets/logos/google_icon.png";
  static const String facebook = "assets/logos/facebook_icon.png";



  //On Boarding Images
  static const String onBoardingImage1= "assets/images/on_boarding_image/bird-flying.gif";
  static const String onBoardingImage2= "assets/images/on_boarding_image/corporate-training.gif";
  static const String onBoardingImage3= "assets/images/on_boarding_image/run.gif";
  
}