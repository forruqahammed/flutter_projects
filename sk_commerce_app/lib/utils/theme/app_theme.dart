import 'package:flutter/material.dart';
import 'package:sk_commerce_app/utils/theme/custom_themes/appbar_theme.dart';
import 'package:sk_commerce_app/utils/theme/custom_themes/bottom_sheet_theme.dart';
import 'package:sk_commerce_app/utils/theme/custom_themes/checkbox_theme.dart';
import 'package:sk_commerce_app/utils/theme/custom_themes/chip_theme.dart';
import 'package:sk_commerce_app/utils/theme/custom_themes/elevated_button_theme.dart';
import 'package:sk_commerce_app/utils/theme/custom_themes/input_decoration_theme.dart';
import 'package:sk_commerce_app/utils/theme/custom_themes/outlined_button_theme.dart';
import 'package:sk_commerce_app/utils/theme/custom_themes/text_theme.dart'; 


class SkAppTheme {
  SkAppTheme._();

  static ThemeData lightTheme = ThemeData(
      useMaterial3: true,
      fontFamily: "Poppins",
      brightness: Brightness.light,
      primaryColor: Colors.blue,
      textTheme: SkTextTheme.lightTextTheme,
      chipTheme: SkChipTheme.lightChipTheme,
      scaffoldBackgroundColor: Colors.white, 
      appBarTheme: SkAppBarTheme.lightAppbarTheme,
      checkboxTheme: SkCheckboxTheme.lightCheckboxTheme,
      bottomSheetTheme: SkBottomSheetTheme.lightBottomSheetTheme,
      elevatedButtonTheme: SkElevatedButtonTheme.lightElevatedButtonTheme,
      outlinedButtonTheme: SkOutlinedButtonTheme.lightOutlinedButtonTheme,  
      inputDecorationTheme: SkTextFormFieldTheme.lightInputDecorationTheme,    
  );

  static ThemeData darkTheme = ThemeData(
       useMaterial3: true,
      fontFamily: "Poppins",
      brightness: Brightness.light,
      primaryColor: Colors.blue,
      textTheme: SkTextTheme.darkTextTheme,
      chipTheme: SkChipTheme.darkChipTheme,
      scaffoldBackgroundColor: Colors.white, 
      appBarTheme: SkAppBarTheme.darkAppbarTheme,
      checkboxTheme: SkCheckboxTheme.darkCheckboxTheme,
      bottomSheetTheme: SkBottomSheetTheme.darkBottomSheetTheme,
      elevatedButtonTheme: SkElevatedButtonTheme.darkElevatedButtonTheme,
      outlinedButtonTheme: SkOutlinedButtonTheme.darkOutlinedButtonTheme,  
      inputDecorationTheme: SkTextFormFieldTheme.darkInputDecorationTheme,
  );

}