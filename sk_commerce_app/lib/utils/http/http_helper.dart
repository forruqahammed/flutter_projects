import 'dart:convert';
import 'package:http/http.dart' as _http;

class SkHttpHelper {
  static const String _baseUrl = "http://skcommerce.com";

  static Future<Map<String, dynamic>> get(String endpoint) async {
    final response = await _http.get(Uri.parse('$_baseUrl/$endpoint'));
    return _handleResponse(response);
  }

  static Future<Map<String, dynamic>> post(
      String endpoint, dynamic data) async {
    final response = await _http.post(
      Uri.parse('$_baseUrl/$endpoint'),
      headers: {'Content-Type': 'application/json'},
      body: json.encode(data),
    );
    return _handleResponse(response);
  }

  static Future<Map<String, dynamic>> put(String endpoint, dynamic data) async {
    final response = await _http.put(
      Uri.parse('$_baseUrl/$endpoint'),
      headers: {'Content-Type': 'application/json'},
      body: json.encode(data),
    );
    return _handleResponse(response);
  }

  static Future<Map<String, dynamic>> delete(String endpoint) async {
    final response = await _http.delete(Uri.parse('$_baseUrl/$endpoint'));
    return _handleResponse(response);
  }

  static Map<String, dynamic> _handleResponse(_http.Response response) {
    // if (response.statusCode == 200) {
    //   return json.decode(response.body);
    // } else {
    //   throw Exception('Failed to load data: ${response.statusCode}');
    // }

    if (response.statusCode == 200 || response.statusCode == 201) {
      // Handle response with content
      return json.decode(response.body);
    } else if (response.statusCode == 204) {
      // 204 means no content to return
      return {};
    } else {
      throw Exception('Failed to load data: ${response.statusCode}');
    }
  }
}
