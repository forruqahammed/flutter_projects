import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sk_commerce_app/features/authentication/screens/onboardig.dart';

class App extends StatelessWidget {
  const App({super.key}); 

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp( 
      debugShowCheckedModeBanner: false,
      themeMode: ThemeMode.system,
      theme: ThemeData(),
      darkTheme: ThemeData(),
      home: const OnBoardingScreen(),
    );
  }
}