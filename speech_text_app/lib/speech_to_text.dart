import 'package:flutter/material.dart';
import 'package:flutter_tts/flutter_tts.dart';
import 'package:speech_to_text/speech_to_text.dart';

class SpeechToTextScreen extends StatefulWidget {
  const SpeechToTextScreen({super.key});

  @override
  State<SpeechToTextScreen> createState() => _SpeechToTextScreenState();
}

class _SpeechToTextScreenState extends State<SpeechToTextScreen> {
  final SpeechToText _speechToText = SpeechToText();
  FlutterTts flutterTts = new FlutterTts();
  bool _speechEnabled = false;
  String _spokenSpeech = '';
  double _confidenceLevel = 0.0;

  @override
  void initState() {
    super.initState();
    initSpeach();
  }

  void initSpeach() async {
    _speechEnabled = await _speechToText.initialize();
    setState(() {});
  }

  void _startListening() async {
    await _speechToText.listen(onResult: _onSpeechResult);
  }

  void _stopListening() async {
    await _speechToText.stop();
    setState(() {
      _confidenceLevel = 0;
    });
  }

  void _onSpeechResult(result) {
    speak(result.recognizedWords);
    setState(() {
      _spokenSpeech = "${result.recognizedWords}";
      _confidenceLevel = result.confidence;
    });
  }
    Future<void> speak(String text) async {
    // await flutterTts.setLanguage('en-US');

    await flutterTts.setLanguage('bn-BD');
    await flutterTts.setSpeechRate(0.5);
    await flutterTts.setPitch(1);
    await flutterTts.speak(text);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.greenAccent,
        title: Text(
          'Speech to Text',
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: Center(
        child: Column(
          children: [
            Text(
              _speechToText.isListening
                  ? "Listening..."
                  : _speechEnabled
                      ? "Tap to the microphone to start listening..."
                      : 'Speech not available',
              style: TextStyle(fontSize: 20),
            ),
            Expanded(
              child: Container(
                padding: EdgeInsets.all(16),
                child: Text(
                  _spokenSpeech
                )
              ),
            ),
            if (_speechToText.isNotListening && _confidenceLevel > 0)
              Padding(
                padding: const EdgeInsets.only(bottom: 8.0),
                child: Text(
                  "Confidence level: " +
                      (_confidenceLevel * 100).toStringAsFixed(2),
                  style: TextStyle(fontSize: 30, fontWeight: FontWeight.w200),
                ),
              ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _speechToText.isListening ? _stopListening : _startListening,
        child: Icon(
          _speechToText.isListening ? Icons.mic_off : Icons.mic,
          color: Colors.white,
        ),
        backgroundColor: Colors.red,
      ),
    );
  }
}
