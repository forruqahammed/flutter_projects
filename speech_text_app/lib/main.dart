import "package:flutter/material.dart";
import "package:speech_text_app/speech_to_text.dart"; 
import "package:speech_text_app/text_to_speech.dart";

void main(){
runApp(MyApp());
} 

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) { 
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title:"Text to Speech",
      theme: ThemeData(primarySwatch: Colors.indigo),
      home: SpeechToTextScreen(),
    );
  } 

}

 