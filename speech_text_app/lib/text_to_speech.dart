import 'package:flutter/material.dart';
import 'package:flutter_tts/flutter_tts.dart';

class TextToSpeech extends StatefulWidget {
   const TextToSpeech({super.key});
 
   @override
   State<TextToSpeech> createState() => _TextToSpeechState();
 }
 
 class _TextToSpeechState extends State<TextToSpeech> {

  FlutterTts flutterTts = new FlutterTts();
  TextEditingController _controller = new TextEditingController();

  Future<void> speak(String text) async {
    // await flutterTts.setLanguage('en-US');

    await flutterTts.setLanguage('bn-BD');
    await flutterTts.setSpeechRate(0.5);
    await flutterTts.setPitch(1);
    await flutterTts.speak(text);
  }


   @override
   Widget build(BuildContext context) {
     return Scaffold(
        appBar: AppBar(
          title: Text('Text to Speech'),
        ),
        body: Container(
          padding: EdgeInsets.only(left: 10, top: 100, right: 10, bottom: 50),

          child: Column(
            children: [
              TextField(
                controller: _controller,
              ),
              ElevatedButton(
                onPressed: () {
                  speak(_controller.text);
                },
                child: Text('Speak'),
                ),
            ],
          ),

        )
     );
   }
 }