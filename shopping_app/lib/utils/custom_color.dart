import 'package:flutter/painting.dart';

class CustomColors{
  static const Color primaryColor = Color(0xFF4C53A5);
   static const Color itemBgColor = Color(0xFFEDECF2);
}