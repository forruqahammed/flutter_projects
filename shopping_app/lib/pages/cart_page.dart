import 'package:flutter/material.dart';
import 'package:shopping_app/utils/custom_color.dart';
import 'package:shopping_app/widgets/cart_appbar_widget.dart';
import 'package:shopping_app/widgets/cart_bottom_navbar_widget.dart';

import '../widgets/cart_items_widget.dart';

class CartPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          CartAppBar(),
          Container(
            height: 700,
            padding: const EdgeInsets.only(top: 15),
            decoration: const BoxDecoration(
              color: Color(0xFFEDECF2),
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(35),
                topRight: Radius.circular(35),
              ),
            ),
            child: Column(
              children: [
                CartItems(),
                Container(
                  // decoration: BoxDecoration(
                  //   borderRadius: BorderRadius.circular(10),
                  // ),
                  margin:
                      const EdgeInsets.symmetric(vertical: 20, horizontal: 15),
                  padding: const EdgeInsets.all(10),
                  child: Row(
                    children: [
                      Container(
                        decoration: BoxDecoration(
                          color: CustomColors.primaryColor,
                          borderRadius: BorderRadius.circular(20),
                        ),
                        child: const Icon(
                          Icons.add,
                          color: Colors.white,
                        ),
                      ),
                      const Padding(padding: EdgeInsets.symmetric(horizontal: 10),
                      child: Text("Add Coupon Code", style: TextStyle(color: CustomColors.primaryColor,
                      fontWeight: FontWeight.bold,
                       fontSize: 14),
                      ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          )
        ],
      ),
      bottomNavigationBar: CartBottomNavbar(),
    );
  }
}
