class CommonRequest {
  final int pageNumber;
  final int pageLimit;

  CommonRequest({required this.pageNumber, required this.pageLimit});
}