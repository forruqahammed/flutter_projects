import 'dart:convert';

import 'package:http/http.dart' as http;

class FakeDataRepository {
  static const String _baseUrl = 'https://jsonplaceholder.typicode.com';

  Future<List<String>> fetchData(int pageNumber, int pageSize) async {
    try {
      final Uri url = Uri.parse('$_baseUrl/posts?_page=$pageNumber&_limit=$pageSize');
      final response = await http.get(url);
      if (response.statusCode == 200) {
        final List<dynamic> data = jsonDecode(response.body);
        return data.map((item) => item['title'].toString()).toList();
      } else {
        throw Exception('Failed to fetch data');
      }
    } catch (e) {
      throw Exception('Failed to fetch data: $e');
    }
  }
}