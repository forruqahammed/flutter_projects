import 'package:cubit_state_management/data/repository/fake_data_repository.dart';
import 'package:cubit_state_management/presentation/cubit/fake_data_cubit.dart';
import 'package:cubit_state_management/presentation/screens/home_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() {
  runApp( MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: BlocProvider(
        create: (context) => DataCubit(FakeDataRepository(), 14),
        child: DataList(),
      ),
    );
  }
}
