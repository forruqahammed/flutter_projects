import 'package:cubit_state_management/presentation/cubit/fake_data_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class DataList extends StatefulWidget {
  @override
  _DataListState createState() => _DataListState();
}

class _DataListState extends State<DataList> {
  final ScrollController _scrollController = ScrollController();
  final _scrollThreshold = 200.0;

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(_onScroll);
    _loadData();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  void _loadData() {
    final cubit = context.read<DataCubit>();
    cubit.fetchData();
  }

  void _onScroll() {
    final maxScroll = _scrollController.position.maxScrollExtent;
    final currentScroll = _scrollController.position.pixels;
    final cubit = context.read<DataCubit>();

    if (maxScroll - currentScroll <= _scrollThreshold) {
      cubit.fetchData();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Data List'),
      ),
      body: BlocBuilder<DataCubit, List<String>>(
        builder: (context, state) {
          if (state.isEmpty) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else {
            return ListView.builder(
              controller: _scrollController,
              itemCount: state.length,
              itemBuilder: (BuildContext context, int index) {
                return ListTile(
                  title: Text(state[index]),
                );
              },
            );
          }
        },
      ),
    );
  }
}