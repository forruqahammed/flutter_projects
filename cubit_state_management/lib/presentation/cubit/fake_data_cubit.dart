import 'package:cubit_state_management/data/repository/fake_data_repository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';


class DataCubit extends Cubit<List<String>> {
  final FakeDataRepository _repository;
  final int _pageSize;
  int _pageNumber = 1;

  DataCubit(this._repository, this._pageSize) : super([]);

  void fetchData() async {
    try {
      final newData = await _repository.fetchData(_pageNumber, _pageSize);
      emit([...state, ...newData]);
      _pageNumber++;
    } catch (e) {
      print('Error fetching data: $e');
    }
  }
}