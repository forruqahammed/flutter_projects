import 'package:card_design/models/category.dart';
import 'package:card_design/styles/colors.dart';
import 'package:card_design/styles/size.dart';
import 'package:card_design/widgets/piechart_custom_painter.dart';
import 'package:flutter/material.dart';

class PieChart extends StatefulWidget {
  @override
  State<PieChart> createState() => _PieChartState();
}

class _PieChartState extends State<PieChart> {

  double total = 0;

  void initState() {
    super.initState();
    category.forEach((element) {
      total+= element['amount'];
    });
  }
  @override
  Widget build(BuildContext context) {
     var width = SizeStyle.getWidth(context);
    return LayoutBuilder(
      builder: (context, constraint) {
        return Container(
          decoration: BoxDecoration(color: AppColors.primaryWhite,
          shape: BoxShape.circle,
          boxShadow: AppColors.neumorpShadow),
          child: Stack(children: [
            Center(
              child: SizedBox(
                width: constraint.maxWidth * 0.6,
                child: CustomPaint(child: Container(),
                foregroundPainter: PieChartCustomPainter(
                  width: constraint.maxWidth * 0.5,
                        categories: category,
                ),
                ),
              ),
            ),
            Center(
                child: Container(
                  width: constraint.maxWidth * .5,
                  decoration: BoxDecoration(
                      color: AppColors.primaryWhite,
                      shape: BoxShape.circle,
                      boxShadow: const [
                         BoxShadow(
                            spreadRadius: 3,
                            blurRadius: 5,
                            offset: Offset(3, 3),
                            color:  Colors.black38)
                      ]),
                  child: Center(
                      child: Text(
                    "\$" + total.toString(),
                    style: const TextStyle(
                        fontWeight: FontWeight.bold, fontSize: 22),
                  )),
                ),
              )
          ],),
        );
      },
    );
  }
}