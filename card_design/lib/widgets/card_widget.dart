import 'package:card_design/models/card.dart';
import 'package:card_design/styles/colors.dart';
import 'package:card_design/styles/size.dart';
import 'package:card_design/widgets/bankcard.dart';
import 'package:flutter/material.dart';

class CardWidget extends StatefulWidget {
  @override
  _CardWidgetState createState() => _CardWidgetState();
}

class _CardWidgetState extends State<CardWidget> {
  @override
  Widget build(BuildContext context) {
    var height = SizeStyle.getHeight(context);
    var width = SizeStyle.getWidth(context);

    return Column(
      children: [
        Container(
          margin: EdgeInsets.symmetric(horizontal: width / 20),
          alignment: Alignment.centerLeft,
          child: const Text(
            "Card Selected",
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
          ),
        ),
        Expanded(
          child: ListView.builder(
            itemCount: card.length,
            scrollDirection: Axis.horizontal,
            physics: const BouncingScrollPhysics(),
            itemBuilder: (context, index) {
              return Container(
                width: width,
                decoration: BoxDecoration(
                  boxShadow: AppColors.neumorpShadow,
                  color: AppColors.primaryWhite,
                  borderRadius: BorderRadius.circular(20),
                ),
                margin: EdgeInsets.symmetric(
                    horizontal: width / 25, vertical: height / 30),
                child: Stack(
                  children: [
                    Positioned.fill(
                     top: 150,
                     bottom: -200,
                     left: 0,
                      child: Container(
                        decoration: BoxDecoration(boxShadow: [
                          BoxShadow(
                            color: Colors.blue[900]!.withOpacity(0.2),
                            blurRadius: 50,
                            spreadRadius: 2,
                            offset: const Offset(20, 0),
                          ),
                         const BoxShadow(
                              color: Colors.white12,
                              blurRadius: 0,
                              spreadRadius: -2,
                              offset: Offset(0, 0))
                        ], shape: BoxShape.circle, color: Colors.white30),
                      ),
                    ),
                    Positioned.fill(
                       top : -100,
                      bottom: -100,
                      left: -300,
                      child: Container(
                        decoration: BoxDecoration(boxShadow: [
                          BoxShadow(
                            color: Colors.blue[900]!.withOpacity(0.2),
                            blurRadius: 50,
                            spreadRadius: 2,
                            offset: const Offset(20, 0),
                          ),
                        const BoxShadow(
                              color: Colors.white12,
                              blurRadius: 0,
                              spreadRadius: -2,
                              offset: Offset(0, 0))
                        ], shape: BoxShape.circle, color: Colors.white30),
                      ),
                    ),
                    BankCard(card, index),
                  ],
                ),
              );
            },
          ),
        )
      ],
    );
  }
}
