import 'package:card_design/models/category.dart';
import 'package:card_design/styles/colors.dart';
import 'package:flutter/material.dart';

class ChartList extends StatelessWidget {
  const ChartList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: category.map((data) {
          return Container(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
            child: Row(
              children: [
                Container(
                  margin: const EdgeInsets.only(right: 10),
                  width: 10,
                  height: 10,
                  decoration: BoxDecoration(
                      color: AppColors.pieColors[category.indexOf(data)],
                      shape: BoxShape.circle),
                ),
                Text(
                  data['name'],
                  style: const TextStyle(fontSize: 12.0),
                )
              ],
            ),
          );
        }).toList(),
      ),
    );
  }
}
