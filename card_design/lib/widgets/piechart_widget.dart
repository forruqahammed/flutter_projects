import 'package:card_design/models/category.dart';
import 'package:card_design/styles/colors.dart';
import 'package:card_design/styles/size.dart';
import 'package:card_design/widgets/chart_list.dart';
import 'package:card_design/widgets/piechart.dart';
import 'package:flutter/material.dart';

class PieChartWidget extends StatelessWidget {
  const PieChartWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
     var height = SizeStyle.getHeight(context);
    var width = SizeStyle.getWidth(context);
    return Expanded(
      child: Row(
        children: [
          const Expanded(
            flex: 5,
            child: ChartList(),
          ),
          Expanded(
            flex: 6,
            child: Padding(
              padding: const EdgeInsets.only(right: 10),
              child: PieChart(),),
          )
        ],
      ), 
      );
  }
}