import 'package:card_design/styles/colors.dart';
import 'package:card_design/styles/size.dart';
import 'package:flutter/material.dart';


class PageHeader extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    var height = SizeStyle.getHeight(context);
    var width = SizeStyle.getWidth(context);
    return Container(
      margin: EdgeInsets.symmetric(horizontal: width/20),
      padding: const EdgeInsets.only(top: 10.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          const Text("Forruq Ahammed", 
          style: TextStyle(fontWeight: FontWeight.bold,
          fontSize: 25),
          ),
          Container(
            height: height/6,
            width: width/6,
            decoration: BoxDecoration(
              color: AppColors.primaryWhite,
              boxShadow: AppColors.neumorpShadow,
              shape: BoxShape.circle
            ),
            child: Stack(children: [
              Center(
                child: Container(
                  margin: const EdgeInsets.all(6.0),
                  decoration: const BoxDecoration(shape: BoxShape.circle, color: Colors.orange),
                ),
              ),
              Center(
                child: Container(
                  margin: const EdgeInsets.all(11.0),
                  decoration: BoxDecoration(color: AppColors.primaryWhite,
                  boxShadow: AppColors.neumorpShadow,
                  shape: BoxShape.circle),
                ),
              ),
              const Center(
                child: Icon(Icons.notifications),
              )
            ],),
          )
        ],
      ),
    );
  }
}