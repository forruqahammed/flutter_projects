import 'package:card_design/styles/colors.dart';
import 'package:card_design/styles/size.dart';
import 'package:card_design/widgets/card_widget.dart';
import 'package:card_design/widgets/header.dart';
import 'package:card_design/widgets/piechart_widget.dart';
import 'package:flutter/material.dart';



class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    var height = SizeStyle.getHeight(context);
    var width = SizeStyle.getWidth(context);
    return Scaffold(
      body: Container(
        color: AppColors.primaryWhite,
        child: Column(
          children: [ 
             const SizedBox(height: 20,),
            Container(height: height/8 ,
            child: PageHeader(),),
              Expanded(
              child:  CardWidget(),
            ),
             const Expanded(
              child: PieChartWidget()
            ),
          ],
        ),
       ),
    );
  }

}