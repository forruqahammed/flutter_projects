import 'package:flutter/material.dart';

import '../pages/login_2.dart';

class DrawerWdget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: [
          const DrawerHeader(
            decoration: BoxDecoration(
              color: Colors.blue,
            ),
            child: Text('Drawer Header'),
          ),
          ListTile(
            title: const Text('Login Page 2'),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => Login2()),
              );
            },
          ),
          ListTile(
            title: const Text('Login Page 3'),
            onTap: () {
              Navigator.pop(context);
            },
          ),
        ],
      ),
    );
  }
}
