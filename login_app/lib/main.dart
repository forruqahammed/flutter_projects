import 'package:flutter/material.dart';
import 'package:login_app/pages/login_1.dart';
import 'package:login_app/widget/drawer_widget.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: login1(),
        drawer: DrawerWdget(),
      ),
    );
  }
}
